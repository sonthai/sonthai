
public class Main {

	private static void showOwnerName () {
		System.out.println("Owner: Son Thai");
		System.out.println("Created at: Mar 10th, 2015");
		System.out.println("Position: Software developer");
		System.out.println("************************");
	}
	
	private static void sayHello () {
		System.out.println ("Hello everyone");
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		sayHello ();
		
		showOwnerName ();
	}

}
